module "september-six-project_db" {
  source = "./db"

  name                                  = var.september-six-project_db_name
  engine                                = var.september-six-project_db_engine
  engine_version                        = var.september-six-project_db_engine_version
  instance_class                        = var.september-six-project_db_instance_class
  storage                               = var.september-six-project_db_storage
  user                                  = var.september-six-project_db_user
  password                              = var.september-six-project_db_password
  random_password                       = var.september-six-project_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.september-six-project_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
