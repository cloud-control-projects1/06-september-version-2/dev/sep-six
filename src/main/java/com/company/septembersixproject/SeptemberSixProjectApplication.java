package com.company.septembersixproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class SeptemberSixProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeptemberSixProjectApplication.class, args);
    }
}
